# Kohana(3.3) RESTapi Module
==================

## Summary

Реализация api в виде модуля, который следует идеологии RESTfull [http://ru.wikipedia.org/wiki/REST]

Навеяно после прочтения статьи на хабре [http://habrahabr.ru/post/181988/] [http://habrahabr.ru/post/45458/]

Собран на Kohana 3.3 [http://kohanaframework.org/3.3/guide/]


Currently it is hardcoded to only respond in JSON (application/json).

при обращение к your-domain.com/api/* срабатывает роутер который находится в init.php:

```php
Route::set('RESTapi', 'api(/<controller>(/<action>))')
	->filter(function($route, $params, $request){
		$params['directory'] = 'Api';
        $params['action'] = $params['action'].'_'.strtolower($request->method());
        return $params;
    })
	->defaults(array(
		'controller' => 'welcome',
		'action'     => 'index'
	));
```
при обращение HTTP методом get, он обращает к вызванному классу, его методу action_index_get 

то есть

```php
Get	 	/index.php/api/welcome/index  --> будет направлен на Conroller/Api/Welcome::action_index_get
Post 	/index.php/api/welcome/index  --> будет направлен на Conroller/Api/Welcome::action_index_post
Put 	/index.php/api/welcome/index  --> будет направлен на Conroller/Api/Welcome::action_index_put
Delete	/index.php/api/welcome/index  --> будет направлен на Conroller/Api/Welcome::action_index_delete
Jsonp 	/index.php/api/welcome/index  --> будет направлен на Conroller/Api/Welcome::action_index_jsonp
```

## Установка

### Example Routes

Это просто модуль Kohana, его можно просто поместить директорию modules ващего проекта. Делается просто копирования нескольких файлов или в качестве Git подмодуль:

`git submodule add git@github.com:samkeen/kohana-simple-REST.git modules/simple_rest`


Так же нужно включить его в файле bootstrap.php

```php
# application/bootstrap.php
/**
 * Enable modules. Modules are referenced by a relative or absolute path.
 */
Kohana::modules(array(
  // 'auth'       => MODPATH.'auth',       // Basic authentication
	// 'cache'      => MODPATH.'cache',      // Caching with multiple backends
	// 'codebench'  => MODPATH.'codebench',  // Benchmarking tool
	   'database'   => MODPATH.'database',   // Database access
	// 'image'      => MODPATH.'image',      // Image manipulation
	// 'orm'        => MODPATH.'orm',        // Object Relationship Mapping
	// 'unittest'   => MODPATH.'unittest',   // Unit testing
	// 'userguide'  => MODPATH.'userguide',  // User guide and API documentation
	   'RESTapi'  	=> MODPATH.'RESTapi',  // User guide and API documentation
	));
```

Then add Routes to support RESTful URIs

Что бы создать новый контроллер, достаточно поместить его в Api/

то есть

```php

class Controller_Api_Users extends Controller {

	public function action_index_get()
	{
		$this->response->body('get');
	}

	public function action_index_post()
	{
		$this->response->body('post');
	}

	public function action_index_put()
	{
		$this->response->body('put');
	}

	public function action_index_delete()
	{
		$this->response->body('delete');
	}

	public function action_index_jsonp()
	{
		$this->response->body('jsonp');
	}
}
```
Чтоб посмотреть что нам отвечает наш api при обращения различными запросами, есть сервис

- Live Demonstration: http://restify.io/
- Demo Video: http://vimeo.com/michealmorgan/restify-overview
- API Browser
- Unit Tests

## Version 0.7.0

просто указывает адрес нашего api, и выбираем как будем обращаться
