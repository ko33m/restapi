<?php defined('SYSPATH') or die('No direct script access.');

// Catch-all route for Codebench classes to run
Route::set('RESTapi', 'api(/<controller>(/<action>))')
	->filter(function($route, $params, $request){
		$params['directory'] = 'Api';
        $params['action'] = $params['action'].'_'.strtolower($request->method());
        return $params;
    })
	->defaults(array(
		'controller' => 'welcome',
		'action'     => 'index'
	));