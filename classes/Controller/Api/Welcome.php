<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Api_Welcome extends Controller {

	public function action_index_get()
	{
		$this->response->body('get');
	}

	public function action_index_post()
	{
		$this->response->body('post');
	}

	public function action_index_put()
	{
		$this->response->body('put');
	}

	public function action_index_delete()
	{
		$this->response->body('delete');
	}

	public function action_index_jsonp()
	{
		$this->response->body('jsonp');
	}

} // End Welcome
